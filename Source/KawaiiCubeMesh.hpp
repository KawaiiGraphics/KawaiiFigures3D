#ifndef KAWAIICUBEMESH_HPP
#define KAWAIICUBEMESH_HPP

#include <QMatrix4x4>
#include "KawaiiFigureMesh.hpp"

class KAWAIIFIGURES3D_SHARED_EXPORT KawaiiCubeMesh: public KawaiiFigureMesh
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiCubeMesh);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
public:  
  KawaiiCubeMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33);
  ~KawaiiCubeMesh() = default;

  static KawaiiCubeMesh* createFromMemento(sib_utils::memento::Memento::DataReader &memento);

  static void emplace(KawaiiMesh3D *target, double size);
  static void emplace(KawaiiMesh3D *target, const QMatrix4x4 &trMat, double size);
};

#endif // KAWAIICUBEMESH_HPP

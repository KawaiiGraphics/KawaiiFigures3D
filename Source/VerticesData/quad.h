const int icount = 6;
const quint32 indices[icount] = {
	 0, 3, 1,  1, 3, 2
};

const int vcount = 4;
const vertex vertices[vcount] = {
	{{-size, 0,-size, 1}, {0.0f,1.0f}, { 0.0f, 1.0f, 0.0f}},
	{{ size, 0,-size, 1}, {1.0f,1.0f}, { 0.0f, 1.0f, 0.0f}},
	{{ size, 0, size, 1}, {1.0f,0.0f}, { 0.0f, 1.0f, 0.0f}},
	{{-size, 0, size, 1}, {0.0f,0.0f}, { 0.0f, 1.0f, 0.0f}}
};

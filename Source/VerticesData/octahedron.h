const uint32_t vcount = 24;
const uint32_t icount = 24;

const vertex vertices[vcount] = {
    {{0*size, 0*size, 1*size, 1.0}, {0.5, 1}, {0.57735, 0.57735, 0.57735}},
    {{1*size, 6.12323e-17*size, -3.04659e-08*size, 1.0}, {0, 0}, {0.57735, 0.57735, 0.57735}},
    {{0*size, 1*size, -3.04659e-08*size, 1.0}, {1, 0}, {0.57735, 0.57735, 0.57735}},
    {{0*size, 0*size, 1*size, 1.0}, {0.5, 1}, {0.57735, -0.57735, 0.57735}},
    {{1.22465e-16*size, -1*size, -3.04659e-08*size, 1.0}, {1, 0}, {0.57735, -0.57735, 0.57735}},
    {{1*size, 6.12323e-17*size, -3.04659e-08*size, 1.0}, {0, 0}, {0.57735, -0.57735, 0.57735}},
    {{0*size, 0*size, 1*size, 1.0}, {0.5, 1}, {-0.57735, -0.57735, 0.57735}},
    {{-1*size, -1.83697e-16*size, -3.04659e-08*size, 1.0}, {0, 0}, {-0.57735, -0.57735, 0.57735}},
    {{1.22465e-16*size, -1*size, -3.04659e-08*size, 1.0}, {1, 0}, {-0.57735, -0.57735, 0.57735}},
    {{0*size, 0*size, 1*size, 1.0}, {0.5, 1}, {-0.57735, 0.57735, 0.57735}},
    {{0*size, 1*size, -3.04659e-08*size, 1.0}, {1, 0}, {-0.57735, 0.57735, 0.57735}},
    {{-1*size, -1.83697e-16*size, -3.04659e-08*size, 1.0}, {0, 0}, {-0.57735, 0.57735, 0.57735}},
    {{0*size, 0*size, -1*size, 1.0}, {0.5, 1}, {0.57735, 0.57735, -0.57735}},
    {{0*size, 1*size, -3.04659e-08*size, 1.0}, {1, 0}, {0.57735, 0.57735, -0.57735}},
    {{1*size, 6.12323e-17*size, -3.04659e-08*size, 1.0}, {0, 0}, {0.57735, 0.57735, -0.57735}},
    {{0*size, 0*size, -1*size, 1.0}, {0.5, 1}, {0.57735, -0.57735, -0.57735}},
    {{1*size, 6.12323e-17*size, -3.04659e-08*size, 1.0}, {0, 0}, {0.57735, -0.57735, -0.57735}},
    {{1.22465e-16*size, -1*size, -3.04659e-08*size, 1.0}, {1, 0}, {0.57735, -0.57735, -0.57735}},
    {{0*size, 0*size, -1*size, 1.0}, {0.5, 1}, {-0.57735, -0.57735, -0.57735}},
    {{1.22465e-16*size, -1*size, -3.04659e-08*size, 1.0}, {1, 0}, {-0.57735, -0.57735, -0.57735}},
    {{-1*size, -1.83697e-16*size, -3.04659e-08*size, 1.0}, {0, 0}, {-0.57735, -0.57735, -0.57735}},
    {{0*size, 0*size, -1*size, 1.0}, {0.5, 1}, {-0.57735, 0.57735, -0.57735}},
    {{-1*size, -1.83697e-16*size, -3.04659e-08*size, 1.0}, {0, 0}, {-0.57735, 0.57735, -0.57735}},
    {{0*size, 1*size, -3.04659e-08*size, 1.0}, {1, 0}, {-0.57735, 0.57735, -0.57735}}
};

const uint32_t indices[icount] = {
    0, 1, 2,
    3, 4, 5,
    6, 7, 8,
    9, 10, 11,
    12, 13, 14,
    15, 16, 17,
    18, 19, 20,
    21, 22, 23
};

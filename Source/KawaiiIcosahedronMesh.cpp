#include "KawaiiIcosahedronMesh.hpp"
#include "Vertex.h"

KawaiiIcosahedronMesh::KawaiiIcosahedronMesh(const QMatrix4x4 &trMat, double size):
  KawaiiFigureMesh(trMat, size)
{
# include "VerticesData/icosahedron.h"
  for(size_t i = 0; i < vcount; ++i)
    addRelVertex(vertices[i].position, vertices[i].normal, vertices[i].tex_coord);

  addIndices(indices);
}

KawaiiIcosahedronMesh *KawaiiIcosahedronMesh::createFromMemento(sib_utils::memento::Memento::DataReader &memento)
{
  QMatrix4x4 mat;
  double sz;
  std::tie(mat, sz) = extractMemento(memento);
  return new KawaiiIcosahedronMesh(mat, sz);
}

void KawaiiIcosahedronMesh::emplace(KawaiiMesh3D *target, double size)
{
# include "VerticesData/icosahedron.h"
  for(size_t i = 0; i < vcount; ++i)
    target->addVertex(qVec(vertices[i].position), qVec(vertices[i].normal), qVec(vertices[i].tex_coord));

  addIndicesTo(indices, target);
}

void KawaiiIcosahedronMesh::emplace(KawaiiMesh3D *target, const QMatrix4x4 &trMat, double size)
{
  QMatrix4x4 normMat(trMat.normalMatrix());

# include "VerticesData/icosahedron.h"
  for(size_t i = 0; i < vcount; ++i)
    target->addVertex(trMat * qVec(vertices[i].position), normMat.map(qVec(vertices[i].normal)), qVec(vertices[i].tex_coord));

  addIndicesTo(indices, target);
}

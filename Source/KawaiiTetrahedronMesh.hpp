#ifndef KAWAIITETRAHEDRONMESH_HPP
#define KAWAIITETRAHEDRONMESH_HPP

#include <QMatrix4x4>
#include "KawaiiFigureMesh.hpp"

class KAWAIIFIGURES3D_SHARED_EXPORT KawaiiTetrahedronMesh: public KawaiiFigureMesh
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiTetrahedronMesh);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
public:  
  KawaiiTetrahedronMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33);
  ~KawaiiTetrahedronMesh() = default;

  static KawaiiTetrahedronMesh* createFromMemento(sib_utils::memento::Memento::DataReader &memento);

  static void emplace(KawaiiMesh3D *target, double size);
  static void emplace(KawaiiMesh3D *target, const QMatrix4x4 &trMat, double size);
};

#endif // KAWAIITETRAHEDRONMESH_HPP

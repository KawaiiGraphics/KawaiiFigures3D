#include <QRegularExpression>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include "KawaiiCubeMesh.hpp"
#include "KawaiiTorusMesh.hpp"
#include "KawaiiIcosahedronMesh.hpp"
#include "KawaiiPlaneMesh.hpp"
#include "KawaiiQuadMesh.hpp"
#include "KawaiiSphereMesh.hpp"
#include "KawaiiOctahedronMesh.hpp"
#include "KawaiiTetrahedronMesh.hpp"

KAWAII_COMMON_IMPLEMENT(KawaiiCubeMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiTorusMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiIcosahedronMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiPlaneMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiQuadMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiSphereMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiOctahedronMesh::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiTetrahedronMesh::reg);

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createCubeMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiCubeMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createTorusMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiTorusMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createIcosahedronMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiIcosahedronMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createPlaneMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiPlaneMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createQuadMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiQuadMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createSphereMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiSphereMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createOctahedronMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiOctahedronMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiMesh3D* createTetrahedronMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  return parent->createChild<KawaiiTetrahedronMesh>(trMat, size);
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createCube(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createCubeMesh(trMat, size, result)->setObjectName("KawaiiCube_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createTorus(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createTorusMesh(trMat, size, result)->setObjectName("KawaiiTorus_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createIcosahedron(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createIcosahedronMesh(trMat, size, result)->setObjectName("KawaiiIcosahedron_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createPlane(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createPlaneMesh(trMat, size, result)->setObjectName("KawaiiPlane_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createQuad(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createQuadMesh(trMat, size, result)->setObjectName("KawaiiQuad_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createSphere(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createSphereMesh(trMat, size, result)->setObjectName("KawaiiSphere_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createOctahedron(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createOctahedronMesh(trMat, size, result)->setObjectName("KawaiiOctahedron_mesh");
  return result;
}

extern "C" KAWAIIFIGURES3D_SHARED_EXPORT KawaiiModel3D* createTetrahedron(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33, KawaiiDataUnit *parent = nullptr)
{
  auto result = parent->createChild<KawaiiModel3D>();
  createTetrahedronMesh(trMat, size, result)->setObjectName("KawaiiTetrahedron_mesh");
  return result;
}

namespace
{
  enum class FigureType
  {
    Cube,
    Torus,
    Icosahedron,
    Plane,
    Quad,
    Sphere,
    Octahedron,
    Tetrahedron,
    None
  };

  std::pair<FigureType, double> strToFigureType(const QString &str)
  {
    auto getSz = [&str](const QString &prefix) {
        QRegularExpression reg(QString(":/%1(?:_x(\\d*(?:\\.\\d+)?))?").arg(prefix));
        if(auto match = reg.match(str); match.hasMatch())
          {
            bool ok;
            if(double f = match.captured(1).toDouble(&ok); ok)
              return f;
            else
              return 0.33;
          }
        return static_cast<double>(0);
    };

    if(float sz = getSz("cube"); sz)
      return {FigureType::Cube, sz};
    else if(float sz = getSz("torus"); sz)
      return {FigureType::Torus, sz};
    else if(float sz = getSz("icosahedron"); sz)
      return {FigureType::Icosahedron, sz};
    else if(float sz = getSz("plane"); sz)
      return {FigureType::Plane, sz};
    else if(float sz = getSz("quad"); sz)
      return {FigureType::Quad, sz};
    else if(float sz = getSz("sphere"); sz)
      return {FigureType::Sphere, sz};
    else if(float sz = getSz("octahedron"); sz)
      return {FigureType::Octahedron, sz};
    else if(float sz = getSz("tetrahedron"); sz)
      return {FigureType::Tetrahedron, sz};
    else
      return {FigureType::None, 0};
  }
}


bool loadFigure_fname(const QString &fileName, KawaiiDataUnit *target)
{
  if(!fileName.startsWith(":/"))
    return false;

  if(auto mesh = dynamic_cast<KawaiiMesh3D*>(target); mesh)
    {
      auto figureType = strToFigureType(fileName);
      if(!figureType.second)
        return false;
      switch(figureType.first)
        {
        case FigureType::Cube:
          KawaiiCubeMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Torus:
          KawaiiTorusMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Icosahedron:
          KawaiiIcosahedronMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Plane:
          KawaiiPlaneMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Quad:
          KawaiiQuadMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Sphere:
          KawaiiSphereMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Octahedron:
          KawaiiOctahedronMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::Tetrahedron:
          KawaiiTetrahedronMesh::emplace(mesh, figureType.second);
          return true;
        case FigureType::None:
          return false;
        }
    } else
    if(auto model = dynamic_cast<KawaiiModel3D*>(target); model)
      {
        auto figureType = strToFigureType(fileName);
        if(!figureType.second)
          return false;
        switch(figureType.first)
          {
          case FigureType::Cube:
            model->createChild<KawaiiCubeMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Torus:
            model->createChild<KawaiiTorusMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Icosahedron:
            model->createChild<KawaiiIcosahedronMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Plane:
            model->createChild<KawaiiPlaneMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Quad:
            model->createChild<KawaiiQuadMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Sphere:
            model->createChild<KawaiiSphereMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Octahedron:
            model->createChild<KawaiiOctahedronMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::Tetrahedron:
            model->createChild<KawaiiTetrahedronMesh>(QMatrix4x4(), figureType.second);
            return true;
          case FigureType::None:
            return false;
          }
      }
  return false;
}

bool loadFigure_fname(const QString &fileName, const QMatrix4x4 &trMat, KawaiiDataUnit *target)
{
  if(!fileName.startsWith(":/"))
    return false;

  if(trMat.isIdentity())
    return loadFigure_fname(fileName, target);

  if(auto mesh = dynamic_cast<KawaiiMesh3D*>(target); mesh)
    {
      auto figureType = strToFigureType(fileName);
      if(!figureType.second)
        return false;
      switch(figureType.first)
        {
        case FigureType::Cube:
          KawaiiCubeMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Torus:
          KawaiiTorusMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Icosahedron:
          KawaiiIcosahedronMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Plane:
          KawaiiPlaneMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Quad:
          KawaiiQuadMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Sphere:
          KawaiiSphereMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Octahedron:
          KawaiiOctahedronMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::Tetrahedron:
          KawaiiTetrahedronMesh::emplace(mesh, trMat, figureType.second);
          return true;
        case FigureType::None:
          return false;
        }
    } else
    if(auto model = dynamic_cast<KawaiiModel3D*>(target); model)
      {
        auto figureType = strToFigureType(fileName);
        if(!figureType.second)
          return false;
        switch(figureType.first)
          {
          case FigureType::Cube:
            model->createChild<KawaiiCubeMesh>(trMat, figureType.second);
            return true;
          case FigureType::Torus:
            model->createChild<KawaiiTorusMesh>(trMat, figureType.second);
            return true;
          case FigureType::Icosahedron:
            model->createChild<KawaiiIcosahedronMesh>(trMat, figureType.second);
            return true;
          case FigureType::Plane:
            model->createChild<KawaiiPlaneMesh>(trMat, figureType.second);
            return true;
          case FigureType::Quad:
            model->createChild<KawaiiQuadMesh>(trMat, figureType.second);
            return true;
          case FigureType::Sphere:
            model->createChild<KawaiiSphereMesh>(trMat, figureType.second);
            return true;
          case FigureType::Octahedron:
            model->createChild<KawaiiOctahedronMesh>(trMat, figureType.second);
            return true;
          case FigureType::Tetrahedron:
            model->createChild<KawaiiTetrahedronMesh>(trMat, figureType.second);
            return true;
          case FigureType::None:
            return false;
          }
      }
  return false;
}

namespace {
  KawaiiDataUnit::LoadhookDescr assetLoadHook(QOverload<const QString&, const QMatrix4x4&, KawaiiDataUnit*>::of(&loadFigure_fname));
}

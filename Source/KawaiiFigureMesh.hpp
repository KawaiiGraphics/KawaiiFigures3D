#ifndef KAWAIIFIGUREMESH_HPP
#define KAWAIIFIGUREMESH_HPP

#include <QMatrix4x4>
#include "KawaiiFigures3D_global.hpp"
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>

class KAWAIIFIGURES3D_SHARED_EXPORT KawaiiFigureMesh: public KawaiiMesh3D
{
  QMatrix4x4 trMat;
  QMatrix4x4 normMat;

protected:
  double size;

public:
  KawaiiFigureMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 1.0);
  ~KawaiiFigureMesh() = default;

protected:
  void addRelVertex(const QVector4D &pos, const QVector3D &normal, const QVector3D &texcoord);
  void addRelVertex(const double (&pos)[4], const double (&normal)[3], const double (&texcoord)[2]);

  static std::pair<QMatrix4x4, double> extractMemento(sib_utils::memento::Memento::DataReader &memento);

  template<typename T, size_t icount>
  static void addIndicesTo(const T (&indices)[icount], KawaiiMesh3D *target)
  {
    static_assert (std::is_integral_v<T>, "T must be integral!");
    static_assert (icount%3 == 0, "icount must be a multiple of 3!");
    for(size_t i = 0; i < icount / 3; ++i)
      {
        const size_t i3 = 3*i;
        target->addTriangle(indices[i3], indices[i3+1], indices[i3+2]);
      }
  }

  template<typename T, size_t icount>
  inline void addIndices(const T (&indices)[icount])
  {
    addIndicesTo(indices, this);
  }

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
};

#endif // KAWAIIFIGUREMESH_HPP

#include "KawaiiFigureMesh.hpp"
#include <sib_utils/Strings.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include "Vertex.h"

KawaiiFigureMesh::KawaiiFigureMesh(const QMatrix4x4 &trMat, double size):
  KawaiiMesh3D(),
  trMat(trMat),
  normMat(trMat.normalMatrix()),
  size(size)
{ }

void KawaiiFigureMesh::addRelVertex(const QVector4D &pos, const QVector3D &normal, const QVector3D &texcoord)
{
  addVertex(trMat * pos, normMat.map(normal), texcoord);
}

void KawaiiFigureMesh::addRelVertex(const double (&pos)[4], const double (&normal)[3], const double (&texcoord)[2])
{
  addRelVertex(qVec(pos), qVec(normal), qVec(texcoord));
}

std::pair<QMatrix4x4, double> KawaiiFigureMesh::extractMemento(sib_utils::memento::Memento::DataReader &memento)
{
  double sz;
  bool szReaded = false;
  memento.readText("size", [&sz, &szReaded] (const QString &text) {
      double tmp = text.toFloat(&szReaded);
      if(szReaded)
        sz = tmp;
    });

  if(!szReaded)
    memento.read("size", [&sz, &szReaded] (const QByteArray &bytes) {
        if(static_cast<size_t>(bytes.size()) >= sizeof(sz))
          {
            sz = *reinterpret_cast<const double*>(bytes.constData());
            szReaded = true;
          }
      });

  bool matReaded = false;
  QMatrix4x4 mat;

  memento.readText("trMat", [&mat, &matReaded] (const QString &text) {
      QStringView str(text);
      QVector<QStringView> lines = str.split('\n', Qt::SkipEmptyParts);
      QVector<QVector<QStringView>> terms(lines.size());
#     pragma omp parallel shared(terms)
      {
#       pragma omp for
        for(int i = 0; i < terms.size(); ++i)
          terms[i] = lines.at(i).split(' ', Qt::SkipEmptyParts);

#       pragma omp for
        for(auto i = 0; i < terms.size(); ++i)
          for(int j = 0; j < terms.at(i).size(); ++j)
            {
              bool ok = true;
              float f = terms.at(i).at(j).toFloat(&ok);
              mat(j, i) = ok? f: 0.0;
              if(ok)
                matReaded = true;
            }
      }
    });

  if(!matReaded)
    memento.read("trMat", [&mat, &matReaded] (const QByteArray &bytes) {
        if(static_cast<size_t>(bytes.size()) >= sizeof(mat))
          {
            mat = *reinterpret_cast<const QMatrix4x4*>(bytes.constData());
            matReaded = true;
          }
      });

  return {mat, sz};
}

void KawaiiFigureMesh::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
//  glm::mat4 m;
//  for(int i = 0; i < 4; ++i)
//    for(int j = 0; j < 4; ++j)
//      m[i][j] = trMat(j, i);
//  memento->setProperty("MainBlob", QByteArray(reinterpret_cast<const char*>(&m), sizeof(glm::mat4)));
  memento.setData("trMat", QByteArray(reinterpret_cast<const char*>(&trMat), sizeof(trMat)));
  memento.setData("size", QByteArray(reinterpret_cast<const char*>(&size), sizeof(size)));
}

void KawaiiFigureMesh::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  QString trMatStr = "\n";
  for(int i = 0; i < 4; ++i)
    for(int j = 0; j < 4; ++j)
      {
        trMatStr += QString::number(trMat(j,i), 'f', 10);
        trMatStr += (j != 3)? ' ': '\n';
      }
  memento.setData("trMat", trMatStr);
  memento.setData("size", QString::number(size, 'f', 10));
}

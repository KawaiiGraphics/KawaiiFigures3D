#ifndef KAWAIISPHEREMESH_HPP
#define KAWAIISPHEREMESH_HPP

#include <QMatrix4x4>
#include "KawaiiFigureMesh.hpp"

class KAWAIIFIGURES3D_SHARED_EXPORT KawaiiSphereMesh: public KawaiiFigureMesh
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiSphereMesh);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
public:  
  KawaiiSphereMesh(const QMatrix4x4 &trMat = QMatrix4x4(), double size = 0.33);
  ~KawaiiSphereMesh() = default;

  static KawaiiSphereMesh* createFromMemento(sib_utils::memento::Memento::DataReader &memento);

  static void emplace(KawaiiMesh3D *target, double size);
  static void emplace(KawaiiMesh3D *target, const QMatrix4x4 &trMat, double size);
};

#endif // KAWAIISPHEREMESH_HPP
